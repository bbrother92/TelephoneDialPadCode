# Test cases for TelephoneDialPad

## Test approach

Testing all possible combinations would require creating test cases in a quantity equivalent to N raised to the power of
K(length of input), making exhaustive testing impractical.
However, it's possible to check cases for short-length inputs, and for other cases, we can use pairwise testing (110 cases)

# Test cases:

| #   | Description         | Steps                 | Expected Result                                                        | Priority |
|-----|---------------------|-----------------------|------------------------------------------------------------------------|----------|
| 01  | Positive tests:     | input:"0"             | class TelephoneDialPad returns: "0"                                    | High     |
|     |                     | "1"                   | "1"                                                                    | High     |
|     |                     | "01"                  | "01"                                                                   | High     |
|     |                     | "010"                 | "010"                                                                  | High     |
|     |                     | "0001"                | "0001"                                                                 | High     |
|     |                     | "1000"                | "1000"                                                                 | High     |
|     |                     | "1000"                | "1000"                                                                 | High     |
|     |                     | "000"                 | "000"                                                                  | High     |
| 09  |                     | "0000000000"          | "0000000000"                                                           | High     |
| 10  |                     | "12"                  | "1A", "1B", "1C"                                                       | High     |
|     |                     | "02"                  | "0A", "0B", "0C"                                                       | High     |
|     |                     | "002"                 | "00A", "00B", "00C"                                                    | High     |
|     |                     | "112"                 | "11A", "11B", "11C"                                                    | High     |
|     |                     | "21"                  | "A0", "B0", "C0"                                                       | High     |
|     |                     | "200"                 | "A00", "B00", "C00"                                                    | High     |
|     |                     | "200"                 | "A00", "B00", "C00"                                                    | High     |
| 17  |                     | "211"                 | "A11", "B11", "C11"                                                    | High     |
|     |                     | "211"                 | "A11", "B11", "C11"                                                    | High     |
|     |                     | "211"                 | "A11", "B11", "C11"                                                    | High     |
| 20  |                     | "74"                  | "PG", "PH", "PI", "QG", "QH", "QI", "RG", "RH", "RI", "SG", "SH", "SI" | High     |
|     |                     | "47"                  | "GP", "GQ", "GR", "GS", "HP", "HQ", "HR", "HS", "IP", "IQ", "IR", "IS" | High     |
|     | Negative cases:     |                       |                                                                        |          |
| 22  | empty               | ""                    | should throw Exception                                                 | Medium   |
|     | empty               | "  "                  | should throw Exception                                                 | Medium   |
|     | Integer.MAX_VALUE+1 | "21474836472"         | should process valid string                                            | Medium   |
|     | Long.MAX_VALUE      | "9223372036854775807" | should throw Exception                                                 | Low      |
|     | invalid             | "-0"                  | should throw Exception                                                 | Medium   |
|     | invalid             | "-1"                  | should throw Exception                                                 | Medium   |
|     | spaces              | " "                   | should throw Exception                                                 | Medium   |
|     | trailing spaces     | " 789 "               | should throw Exception                                                 | Medium   |
|     | spaces              | "9 4 5"               | should throw Exception                                                 | Medium   |
|     | invalid             | "\t"                  | should throw Exception                                                 | Medium   |
|     | invalid             | "\n"                  | should throw Exception                                                 | Medium   |
|     | invalid             | "~`!@#$%^&*()-_"      | should throw Exception                                                 | Medium   |
|     | invalid             | "FF"                  | should throw Exception                                                 | Low      |
|     | invalid             | "1A"                  | should throw Exception                                                 | Low      |
|     | invalid             | "0A"                  | should throw Exception                                                 | Low      |
|     | invalid             | "A0"                  | should throw Exception                                                 | Low      |
|     | invalid             | "XLVI"                | should throw Exception                                                 | Low      |
|     | float               | "99.9"                | should throw Exception                                                 | Medium   |
|     | float               | ".001"                | should throw Exception                                                 | Medium   |
|     | Chinese numerals    | "五"                   | should throw Exception                                                 | Medium   |
|     | Chinese numerals    | "〇"                   | should throw Exception                                                 | Medium   |
| 43  | binary              | "0b00110001"          | should throw Exception                                                 | Medium   |

