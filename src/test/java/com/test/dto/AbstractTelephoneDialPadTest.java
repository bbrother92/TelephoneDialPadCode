package com.test.dto;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import io.qameta.allure.Step;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.time.temporal.ChronoUnit.SECONDS;

public abstract class AbstractTelephoneDialPadTest {
    public static GenericContainer<?> container = new GenericContainer<>("gradle:jdk11-focal");


    @BeforeSuite
    public static void startContainer() {
        container.setWaitStrategy(Wait.defaultWaitStrategy()
                .withStartupTimeout(Duration.of(100, SECONDS)));
        container.start();
    }

    @AfterSuite
    public static void stopContainer() {
        container.stop();
    }

    protected List<String> prepareExpectedCombinations(String digitDialInput) {
        List<Set<String>> subsets = new ArrayList<>();
        char[] chars = digitDialInput.toCharArray();

        for (char c : chars) {
            switch (c) {
                case '0':
                    subsets.add(ImmutableSet.of("0"));
                    break;
                case '1':
                    subsets.add(ImmutableSet.of("1"));
                    break;
                case '2':
                    subsets.add(ImmutableSet.of("A", "B", "C"));
                    break;
                case '3':
                    subsets.add(ImmutableSet.of("D", "E", "F"));
                    break;
                case '4':
                    subsets.add(ImmutableSet.of("G", "H", "I"));
                    break;
                case '5':
                    subsets.add(ImmutableSet.of("J", "K", "L"));
                    break;
                case '6':
                    subsets.add(ImmutableSet.of("M", "N", "O"));
                    break;
                case '7':
                    subsets.add(ImmutableSet.of("P", "Q", "R", "S"));
                    break;
                case '8':
                    subsets.add(ImmutableSet.of("T", "U", "V"));
                    break;
                case '9':
                    subsets.add(ImmutableSet.of("W", "X", "Y", "Z"));
                    break;
            }
        }

        List<String> listOfCombinations = Sets.cartesianProduct(subsets).stream()
                .map(set -> String.join("", set))
                .collect(Collectors.toList());
        return listOfCombinations;
    }

    @Step("Calculate expected number of combinations [{parameter}]")
    protected int calculateExpectedNumberOfCombinations(String digitDialInput) {
        int numberOfCombinations = 1;
        char[] chars = digitDialInput.toCharArray();

        for (char c : chars) {
            switch (c) {
                case '0':
                case '1':
                    numberOfCombinations *= 1;
                    break;
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '8':
                    numberOfCombinations *= 3;
                    break;
                case '7':
                case '9':
                    numberOfCombinations *= 4;
                    break;
            }
        }
        return numberOfCombinations;
    }
}
