package com.test.dto;

import com.google.common.collect.ImmutableList;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

/** Positive test cases
 * @see   <a href="TestCases.md">TestCases.md</a>
 * @author Viven Andzuana
 */
public class TelephoneDialPadNegativeTest extends AbstractTelephoneDialPadTest {

    @DataProvider(name = "negativeCases")
    public Object[][] inputAndExpectedNegativeCases() {
        return new Object[][]{
                {"  "},
                {"-0"},
                {"-1"},
                {" "},
                {" 789 "},
                {"9 4 5"},
                {"\t"},
                {"\n"},
                {"~`!@#$%^&*()-_"},
                {"FF"},
                {"1A"},
                {"0A"},
                {"A0"},
                {"XLVI"},
                {"99.9"},
                {".001"},
                {"五"},
                {"〇"},
                {"0b00110001"}
        };
    }

    @TmsLink("TestCases.md")
    @Story("Negative test cases")
    @Test(dataProvider = "negativeCases")
    public void testNegativeCases(String dialInput) {
        assertThatExceptionOfType(NumberFormatException.class)
                .isThrownBy(() -> {
                    TelephoneDialPad.retrieveCombinations(dialInput);
                });
    }

    @TmsLink("TestCases.md")
    @Story("Big values")
    @Test
    public void testBigValue() {
        String dialInput = "21474836472";
        List<String> actualList = TelephoneDialPad.retrieveCombinations(dialInput);
        assertThat(calculateExpectedNumberOfCombinations(dialInput)).isEqualTo(actualList.size());
        assertThat(actualList).as("Should have same combinations as expected").containsExactlyInAnyOrderElementsOf(prepareExpectedCombinations(dialInput));
    }

    @TmsLink("TestCases.md")
    @Story("Big values")
    @Test
    public void testBigValueOutOfMemory() {
        String dialInput = "9223372036854775807";
        assertThatExceptionOfType(OutOfMemoryError.class)
                .isThrownBy(() -> {
                    TelephoneDialPad.retrieveCombinations(dialInput);
                });
    }

    @TmsLink("TestCases.md")
    @Story("Negative test cases")
    @Test
    public void testEmptyString() {
        assertThatExceptionOfType(StringIndexOutOfBoundsException.class)
                .isThrownBy(() -> {
                    TelephoneDialPad.retrieveCombinations("");
                });
    }
}