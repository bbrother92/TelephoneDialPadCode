package com.test.dto;

import com.google.common.collect.ImmutableList;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/** Positive test cases
 * @see   <a href="TestCases.md">TestCases.md</a>
 * @author Viven Andzuana
 */
public class TelephoneDialPadPositivePairwiseTest extends AbstractTelephoneDialPadTest {

    @DataProvider(name = "pairwiseCombinations")
    public Object[][] inputAndExpectedDatapairwiseCombinations() {
        return new Object[][]{
                {"577"}, {"679"}, {"358"}, {"240"}, {"183"}, {"169"}, {"666"}, {"225"}, {"371"}, {"638"}, {"741"}, {
                "360"}, {"962"}, {"145"}, {"506"}, {"830"}, {"047"}, {"293"}, {"026"}, {"642"}, {"475"}, {"487"},
                {"680"}, {"432"}, {"446"}, {"395"}, {"621"}, {"702"}, {"418"}, {"239"}, {"088"}, {"553"}, {"286"}, {
                "585"}, {"104"}, {"544"}, {"913"}, {"409"}, {"059"}, {"770"}, {"336"}, {"764"}, {"522"}, {"451"},
                {"861"}, {"519"}, {"014"}, {"981"}, {"843"}, {"091"}, {"329"}, {"855"}, {"531"}, {"807"}, {"211"}, {
                "978"}, {"957"}, {"990"}, {"317"}, {"267"}, {"303"}, {"254"}, {"463"}, {"799"}, {"615"}, {"694"},
                {"934"}, {"733"}, {"816"}, {"828"}, {"905"}, {"598"}, {"152"}, {"208"}, {"424"}, {"892"}, {"717"}, {
                "723"}, {"889"}, {"148"}, {"874"}, {"496"}, {"072"}, {"560"}, {"073"}, {"035"}, {"176"}, {"400"},
                {"272"}, {"001"}, {"382"}, {"788"}, {"197"}, {"112"}, {"637"}, {"130"}, {"068"}, {"010"}, {"121"}, {
                "650"}, {"384"}, {"920"}, {"755"}, {"527"}, {"949"}, {"603"}, {"665"}, {"956"}, {"766"}, {"348"}
        };
    }

    @TmsLink("TestCases.md")
    @Story("Test cases generated using pairwise")
    @Test(dataProvider = "pairwiseCombinations")
    public void testPairWiseCombinations(String dialInput) {
        List<String> actualList = TelephoneDialPad.retrieveCombinations(dialInput);
        assertThat(calculateExpectedNumberOfCombinations(dialInput)).isEqualTo(actualList.size());
        assertThat(actualList).as("Should have same combinations as expected").containsExactlyInAnyOrderElementsOf(prepareExpectedCombinations(dialInput));
    }
}