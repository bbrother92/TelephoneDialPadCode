package com.test.dto;

import com.google.common.collect.ImmutableList;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/** Positive test cases
 * @see   <a href="TestCases.md">TestCases.md</a>
 * @author Viven Andzuana
 */
@Story("Basic positive cases")
public class TelephoneDialPadPositiveTest extends AbstractTelephoneDialPadTest {

    @DataProvider(name = "zerosAndOnes")
    public Object[][] inputAndExpectedDataZerosAndOnes() {
        return new Object[][]{{"0", ImmutableList.of("0")},
                {"1", ImmutableList.of("1")},
                {"0001", ImmutableList.of("0001")},
                {"1000", ImmutableList.of("1000")},
                {"010", ImmutableList.of("010")},
                {"000", ImmutableList.of("000")},
        };
    }

    @TmsLink("TestCases.md")
    @Test(dataProvider = "zerosAndOnes")
    public void testRetrieveCombinations(String dialInput, List<String> expectedCombinations) {
        List<String> actualList = TelephoneDialPad.retrieveCombinations(dialInput);
        assertThat(calculateExpectedNumberOfCombinations(dialInput)).isEqualTo(actualList.size());
        assertThat(actualList).as("Should have same combinations as expected").containsExactlyInAnyOrderElementsOf(expectedCombinations);
    }

    @DataProvider(name = "simpleCombinations")
    public Object[][] inputAndExpectedDataSimpleCombinations() {
        return new Object[][]{{"12", ImmutableList.of("1A", "1B", "1C")},
                {"02", ImmutableList.of("0A", "0B", "0C")},
                {"002", ImmutableList.of("00A", "00B", "00C")},
                {"112", ImmutableList.of("11A", "11B", "11C")},
                {"21", ImmutableList.of("A1", "B1", "C1")},
                {"20", ImmutableList.of("A0", "B0", "C0")},
                {"200", ImmutableList.of("A00", "B00", "C00")},
                {"211", ImmutableList.of("A11", "B11", "C11")},
        };
    }

    @TmsLink("TestCases.md")
    @Test(dataProvider = "simpleCombinations")
    public void testSimpleCombinations(String dialInput, List<String> expectedCombinations) {
        List<String> actualList = TelephoneDialPad.retrieveCombinations(dialInput);
        assertThat(calculateExpectedNumberOfCombinations(dialInput)).isEqualTo(actualList.size());
        assertThat(actualList).as("Should have same combinations as expected").containsExactlyInAnyOrderElementsOf(expectedCombinations);
    }

    @DataProvider(name = "mediumSizeCombinations")
    public Object[][] inputAndExpectedDataMediumSizeCombinations() {
        return new Object[][]{{"74", ImmutableList.of("PG", "PH", "PI", "QG", "QH", "QI", "RG", "RH", "RI", "SG", "SH"
                , "SI")},
                {"47", ImmutableList.of("GP", "GQ", "GR", "GS", "HP", "HQ", "HR", "HS", "IP", "IQ", "IR", "IS")},
        };
    }

    @TmsLink("TestCases.md")
    @Test(dataProvider = "mediumSizeCombinations")
    public void testMediumSizeCombinations(String dialInput, List<String> expectedCombinations) {
        List<String> actualList = TelephoneDialPad.retrieveCombinations(dialInput);
        assertThat(calculateExpectedNumberOfCombinations(dialInput)).isEqualTo(actualList.size());
        assertThat(actualList).as("Should have same combinations as expected").containsExactlyInAnyOrderElementsOf(expectedCombinations);
    }
}